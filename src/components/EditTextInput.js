import React from 'react'
import { View, TextInput, StyleSheet, Image } from 'react-native'

export default class EditTextInput extends React.Component {

  state = {
    input : ''
  }

  _getValue () {
    return this.state.input
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={{flexDirection : 'row', alignItems: 'center'}}>
          <Image
          style={{width: 32, height: 32, marginRight: 16}}
          source={this.props.image}
          />
          <TextInput
            underlineColorAndroid='transparent'
            placeholder={this.props.placeholder}
            placeholderTextColor='gray'
            style={styles.text_input}
            onChangeText={(text) => {this.setState({input : text})} }
            value={this.state.input}
          />
        </View>
        <View style={[styles.underline]}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  underline: {
    height: 1,
    backgroundColor: 'gray'
  },
  text_input: {
    flex: 1,
    fontSize: 16,
    paddingTop: 16,
    paddingBottom: 16
  }
})