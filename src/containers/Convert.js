import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native'
import Button from '../components/Button'

export default class Convert extends React.Component {

  state = { fahrenheit : 0, input : '' }

  convertCToF () {
    const input = this.state.input
    if (input.trim() === '') {
      Alert.alert('Input empty')
    } else {
      let inputNumber = Number(input)
      if (typeof inputNumber === 'number' && inputNumber + '' !== 'NaN') {
        this.setState({
          fahrenheit: (1.8 * inputNumber) + 32
        })
      } else {
        Alert.alert('Input number only')
      }
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.text_fahrenheit}>Fahrenheit : {this.state.fahrenheit}</Text>
        <TextInput
          placeholder='Input Celsius'
          placeholderTextColor='#FFFFFF'
          style={styles.text_input}
          onChangeText={(text) => {this.setState({input : text})} }
          value={this.state.input}
        />
         <Button styleText={stylesButton.text} styleBackground={stylesButton.view} title={'Calculate'} onPressButton={() => this.convertCToF()}/>
      </View>
    )
  }
}

const stylesButton = StyleSheet.create({
  view: {
    backgroundColor: 'blue'
  },
  text: {
    color: 'red'
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    margin: 16
  },
  text_fahrenheit: {
    alignSelf: 'center',
    marginBottom: 10,
    fontSize: 20
  },
  text_input: {
    backgroundColor: '#C8C8C8',
    padding: 16,
    marginBottom: 16
  },
  button_layout: {
    backgroundColor: 'red',
  },
  text_button: {
    color: 'white',
    alignSelf: 'center',
    padding: 16,
    fontSize: 20
  }
})
