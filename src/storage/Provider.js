import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export const AppContext = React.createContext()

export default class Provider extends React.Component {
  state = {
    name : '',
    setName: (input_name) => {
      this.setState({
        name : input_name
      })
    }
  }
  render () {
    return (
      <AppContext.Provider value={this.state}>
        {this.props.children}
      </AppContext.Provider>
    )
  }
}
