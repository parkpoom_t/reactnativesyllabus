import React from 'react'
import { View, StyleSheet, Alert, TouchableOpacity, Text } from 'react-native'
import EditTextInput from '../components/EditTextInput'
import Button from '../components/Button'
import {AppContext} from '../storage/Provider'

export default class Register extends React.Component {

  static navigationOptions = {
    title: 'Register'
  }

  inputUsername = null
  inputPassword = null
  inputFirstName = null
  inputLastName = null
  inputPhoneNo = null
  inputEmail = null


  _goToExerciseScreen () {
    this.props.navigation.pop()
  }

  _onPressRegister (context) {
    const data = {
      'first_name': this.inputFirstName._getValue(),
      'last_name': this.inputLastName._getValue(),
      'username': this.inputUsername._getValue(),
      'password': this.inputPassword._getValue(),
      'phone_no': this.inputPhoneNo._getValue(),
      'email': this.inputEmail._getValue()
    }
    fetch('http://45.32.78.77/rest-api/mobile/api/register',{
      method: "POST",
      headers: {
            'Authorization': 'Basic cm5fc3lsbGFidXM6MXFhelhTV0A=',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
      body: JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((responseJson) => {
      debugger
      if(responseJson.res_code !== '0000'){
        Alert.alert(responseJson.res_desc)
      } else {
        Alert.alert('Register Success', null, [
          { text: 'OK', onPress: () => this._goToExerciseScreen()}
        ])
        context.setName(responseJson.first_name + ' ' + responseJson.last_name)
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render () {
    return (
      <AppContext.Consumer>
        {(context) =>
        <View style={styles.container}>
          <View>
            <EditTextInput ref={(ref) => this.inputUsername = ref} 
              placeholder={'Username'}
              image={require('../img/ic_account_circle.png')} />
            <EditTextInput ref={(ref) => this.inputPassword = ref} 
              placeholder={'Password'}
              image={require('../img/ic_account_circle.png')} />
              <EditTextInput ref={(ref) => this.inputFirstName = ref} 
              placeholder={'First Name'}
              image={require('../img/ic_account_circle.png')} />
              <EditTextInput ref={(ref) => this.inputLastName = ref} 
              placeholder={'Last Name'}
              image={require('../img/ic_account_circle.png')} />
              <EditTextInput ref={(ref) => this.inputPhoneNo = ref} 
              placeholder={'Phone No'}
              image={require('../img/ic_account_circle.png')} />
              <EditTextInput ref={(ref) => this.inputEmail = ref} 
              placeholder={'Email'}
              image={require('../img/ic_account_circle.png')} />
            </View>
          <Button title={'Register'} onPressButton={() => this._onPressRegister(context)}/>
        </View>
        }
      </AppContext.Consumer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 16,
    justifyContent: 'space-between'
  }
})