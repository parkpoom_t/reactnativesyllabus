import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { AppContext } from '../storage/Provider'

import Button from '../components/Button'

export default class Main extends React.Component {

  static navigationOptions = {
    title: 'Main'
  }

  _goToConvertScreen () {
    this.props.navigation.navigate('Convert')
  }

  _goToListScreen () {
    this.props.navigation.navigate('List')
  }

  render () {
    return (
      <AppContext.Consumer>
        {(context) =>
          <View style={styles.container}>
            <Text style={{fontSize: 16}}>Welcome to Main App</Text>
            <Text>{context.name}</Text>
            <Button title={'Convert C To F'} 
            styleBackground={{backgroundColor : '#ff6699'}} 
            onPressButton={() => this._goToConvertScreen()}/>
            <Button title={'List Movie'} 
            styleBackground={{backgroundColor : '#ff6699'}} 
            onPressButton={() => this._goToListScreen()}/>
          </View>
        }
      </AppContext.Consumer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 16
  }
})
