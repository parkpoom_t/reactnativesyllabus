/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Animated,
  Image,
  Easing,
  Dimensions,
  TouchableOpacity
} from 'react-native';

const width = Dimensions.get('window').width

type Props = {};
export default class Item extends Component<Props> {
  constructor(props){
      super(props)
  }

  componentDidMount(){
    
  }

  onPressButton() {
    // this.animateClick.setValue(0)
    // Animated.timing(
    //   this.animateClick,
    //   {
    //     toValue: 1,
    //     duration: 200
    //   }
    // ).start()
    // this.props.onPress()
  }

  render() {
    return (
    <View style={{flex: 1, padding: 10, flexDirection: 'row'}}>
      <Image
        source={{uri : 'https://mamryzxd.files.wordpress.com/2012/07/logo20major.jpg'}}
        style={[{ width: 50, height: 50 }]}
        resizeMode="contain"
      />

      <View style={{flexDirection: 'column', justifyContent: 'center', paddingLeft: 20}}>
        <Text>{this.props.item.title}</Text>
        <Text>{this.props.item.releaseYear}</Text>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 220,
    borderRadius: 10,
    backgroundColor: "#FFF",
    margin: 16,
    overflow: 'hidden',
    ...Platform.select(
      {
        ios: {
            shadowColor: 'rgba(0,0,0, .2)',
            shadowOffset: { height: 0, width: 0 },
            shadowOpacity: 1,
            shadowRadius: 1
        },
        android: {
          elevation: 1
        }
      }
    )
  },
  cardImage: {
    width: "100%",
    height: "100%",
    borderTopLeftRadius: 10 , borderTopRightRadius : 10
  },
  textDetail: {
    paddingLeft: 16,
    fontSize: 10,
    marginRight: 16
  },
  textTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingBottom: 16
  }
});
