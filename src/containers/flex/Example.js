import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert } from 'react-native';

class Example extends Component {

  static navigationOptions = {
    header: null
  }

  goToExercise() {
    this.props.navigation.navigate('Exercise')
  }

  render() {
    return (
      <View style={styles.container}>

        <View style = {styles.header}>
            <Image
                style={{width: undefined, height: undefined, flex:1}}
                resizeMode='contain'
                source={require('./../../img/smile001.png')}
            />
        </View>

        <View style = {styles.body}>
            <Text style={ styles.titleHeader }>WELCOME MY APPLICATION</Text>
            <View style={styles.bodyGorup}>
                <Text>Create project by. </Text>
                <Text>Nattam Areesiri</Text>
            </View>
            <Text>Learn Basic React Native.</Text>
        </View>

        <View style = {styles.footer}>
            <TouchableOpacity style={styles.Botton} onPress={() => this.goToExercise()}>
                <View >
                    <Text style={styles.titleButton}>
                        Sign In
                    </Text>
                </View>
            </TouchableOpacity>
        </View>

      </View>
    );
  }
}

export default Example;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 16,
  },
  header: {
    flex: 6,
    justifyContent: 'flex-end',
  },
  body: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bodyGorup: {
    flexDirection: 'row',
  },
  footer: {
    flex: 1
  },
  titleHeader: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  Botton: {
      flex: 1,
      backgroundColor: '#ff99bb',
      alignItems: 'center',
      justifyContent: 'center',
  },
  titleButton: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  }
});
