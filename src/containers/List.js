import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Alert, FlatList } from 'react-native'
import Item from '../components/Item'

export default class List extends React.Component {

  static navigationOptions = {
    title: 'Movie'
  }

  state = { list_movie : []}

  componentDidMount () {
    this._getMoviesFromApiAsync()
  }

  _getMoviesFromApiAsync = () => {
    fetch('https://facebook.github.io/react-native/movies.json')
    .then((response) => response.json())
    .then((responseJson) => {
      debugger
      this.setState({list_movie : responseJson.movies});
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {

    return (
      <View style={{flex: 1}}>
        <FlatList
          ItemSeparatorComponent={() => <View style={{ backgroundColor: 'blue', height: 1, marginLeft: 80}} />}
          data={this.state.list_movie}
          renderItem={({item}) => <Item item={item}/>}
        />
      </View>
    )
  }
}