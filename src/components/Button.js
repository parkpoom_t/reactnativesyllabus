import React from 'react'
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native'

export default class Button extends React.Component {

  render () {
    return (
      <View>
        <TouchableOpacity
          style={[styles.button_layout, this.props.styleBackground]}
          onPress={() => this.props.onPressButton()}
          underlayColor='#FFFFFF'>
          <Text style={[styles.text_button, this.props.styleText]}>{this.props.title}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button_layout: {
    marginTop: 16,
    borderRadius: 16,
    backgroundColor: 'red'
  },
  text_button: {
    color: 'white',
    alignSelf: 'center',
    padding: 16,
    fontSize: 20
  }
})
