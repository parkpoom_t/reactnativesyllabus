import React from 'react'
import { View, StyleSheet, Alert, TouchableOpacity, Text } from 'react-native'
import EditTextInput from '../components/EditTextInput'
import Button from '../components/Button'
import {AppContext} from '../storage/Provider'

export default class Login extends React.Component {


  static navigationOptions = {
    title: 'Login'
  }

  inputUsername = null
  inputPassword = null


  _goToMainScreen () {
    this.props.navigation.navigate('Main')
  }

  _onPressLogin (context) {
    const data = {
      'username': this.inputUsername._getValue(),
      'password': this.inputPassword._getValue()
    }
    fetch('http://45.32.78.77/rest-api/mobile/api/login',{
      method: "POST",
      headers: {
            'Authorization': 'Basic cm5fc3lsbGFidXM6MXFhelhTV0A=',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
      body: JSON.stringify(data)
    })
    .then((response) => response.json())
    .then((responseJson) => {
      debugger
      if(responseJson.res_code !== '0000'){
        Alert.alert(responseJson.res_desc)
      } else {
        Alert.alert('Login Success', null, [
          { text: 'OK', onPress: () => this._goToMainScreen()}
        ])
        context.setName(responseJson.first_name + ' ' + responseJson.last_name)
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render () {
    return (
      <AppContext.Consumer>
        {(context) =>
        <View style={styles.container}>
          <View>
            <EditTextInput ref={(ref) => this.inputUsername = ref} 
                placeholder={'Username'}
                image={require('../img/ic_account_circle.png')} />
            <EditTextInput ref={(ref) => this.inputPassword = ref} 
                placeholder={'Password'}
                image={require('../img/ic_lock.png')} />
          </View>
          <Button title={'Login'} styleBackground={{backgroundColor : 'blue'}} onPressButton={() => this._onPressLogin(context)}/>
        </View>
        }
      </AppContext.Consumer>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 16,
    justifyContent: 'space-between'
  }
})