import React, { Component }  from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

class Exercise extends Component {

  static navigationOptions = {
    header: null
  }

  goToLogin() {
    this.props.navigation.navigate('Login')
  }

  goToRegister() {
    this.props.navigation.navigate('Register')
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Image 
            style = {{
              flex: 1,
              width: undefined,
              height: undefined,
            }
            }
            resizeMode='cover'
            source={require('./../../img/home.png')}
          >

          </Image>
        </View>

        <View style={styles.body}>

            <View style={styles.bodyHeader}>
              <Text style={styles.titleheader}>
                WELCOME TO REACT NATIVES
              </Text>
              <Text>
                Open up App.js to start working on your app!
              </Text>
            </View>
            
            <View style={styles.bodyBody}>

              <View style={[styles.viewButton]}>
                <TouchableOpacity onPress={() => this.goToRegister()}>
                  <View style={styles.buttonPink}>
                    <Text style={styles.titleheader}>
                      Register
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.viewButton, {marginTop: 20}]}>
                <TouchableOpacity onPress={() => this.goToLogin()}>
                  <View style={styles.buttonBlue}>
                    <Image 
                      style = {{
                        // flex: 1,
                        width: 25,
                        height: 25,
                        position: 'absolute',
                        marginTop: 11,
                        marginLeft: 10,
                        // justifyContent: 'center',
                        // alignItems: 'center'
                      }
                      }
                      // resizeMode='cover'
                      source={require('./../../img/facebook.png')}
                    />
                    <View style = {styles.viewTextButton}>
                      <Text style={styles.titleheader}>
                        Login
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>

            </View>

        </View>

        <View style={styles.footer}>
          <Text>Don't have account ? </Text>
          <Text style={[{ color: 'blue' }]}>Sign up</Text>
        </View>

      </View>
    );
  }
}

export default Exercise;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  header: {
    flex: 1,
    // backgroundColor: '#99ff99',
  },
  body: {
    flex: 1,
    // backgroundColor: '#ffff99',
  },
  bodyHeader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  bodyBody: {
    flex: 1,
  },
  footer: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  titleheader: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  viewButton: {
    paddingRight: 20, 
    paddingLeft: 20,
  },
  viewTextButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonPink: {
    padding: 10,
    backgroundColor: '#ff6699',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    flexDirection: 'row'
  },
  buttonBlue: {
    padding: 10,
    backgroundColor: '#0066ff',
    // justifyContent: 'center',
    // alignItems: 'center',
    borderRadius: 30,
    flexDirection: 'row'
  }
});
