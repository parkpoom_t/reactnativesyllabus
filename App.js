import React from 'react'
import Convert from './src/containers/Convert'
import Register from './src/containers/Register'
import List from './src/containers/List'

import Example from './src/containers/flex/Example'
import Exercise from './src/containers/flex/Exercise'
import Main from './src/containers/Main'
import Login from './src/containers/Login'

import { createStackNavigator } from 'react-navigation'

import Provider from './src/storage/Provider'

export default class App extends React.Component {
  render () {
    return (
      <Provider>
        <AppStackNavigator />
      </Provider>
    )
  }
}

const AppStackNavigator = createStackNavigator({
  Example: { screen: Example },
  Exercise: { screen: Exercise },
  Register: {screen: Register},
  Login: { screen: Login },
  Main: { screen: Main },
  Convert: { screen: Convert },
  List: { screen: List }
})
